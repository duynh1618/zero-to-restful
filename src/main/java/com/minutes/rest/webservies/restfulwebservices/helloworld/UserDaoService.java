package com.minutes.rest.webservies.restfulwebservices.helloworld;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Component
public class UserDaoService {
    private static List<User> users = new ArrayList<>();
    static int countUser = 0;
    static {
        users.add(new User(++countUser, "Jenni", LocalDate.now().minusYears(30)));
        users.add(new User(++countUser, "Lisa", LocalDate.now().minusYears(27)));
        users.add(new User(++countUser, "Rosie", LocalDate.now().minusYears(28)));
        users.add(new User(++countUser, "Jisso", LocalDate.now().minusYears(29)));
    }

    public List<User> findAll() {
        return users;
    }

    public User findOne(int id) {
        Predicate<? super User> predicate = user -> user.getId().equals(id);
        return users.stream().filter(predicate).findFirst().orElse(null);
    }

    public User createUser(User user) {
        user.setId(++countUser);
        users.add(user);
        return user;
    }

    public void deleteById(int id) {
        Predicate<? super User> predicate = user -> user.getId().equals(id);
        users.removeIf(predicate);
    }
}
