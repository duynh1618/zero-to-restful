package com.minutes.rest.webservies.restfulwebservices.helloworld;


import javax.validation.constraints.*;
import java.time.LocalDate;


public class User {

    @Size(min = 2, message = "Name should have at least 2 characters")
    private String name;
    private Integer id;
    @Past(message = "Birth Date should be in the past")
    private LocalDate birthDate;

    public User( Integer id, String name, LocalDate birthDate) {
        super();
        this.name = name;
        this.id = id;
        this.birthDate = birthDate;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", birthDate=" + birthDate +
                '}';
    }
}
